require 'cli/ui'
require 'json'
require 'json-diff'

require_relative 'lib/print_diff'

CLI::UI::StdoutRouter.enable

input1 = CLI::UI.ask('Input 1:')
input2 = CLI::UI.ask('Input 2:')

json1 = JSON.parse(input1)
json2 = JSON.parse(input2)

diffs = JsonDiff.diff(json1, json2, include_was: true)

print_diff(diffs)