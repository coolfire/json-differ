def print_diff(diffs)
 diffs.each do |d|
    puts d['path']
    case d['op']
    when 'add'
      puts CLI::UI.fmt "{{green: + #{d['value']} }}"
    when 'replace'
      puts CLI::UI.fmt "{{red: - #{d['was']}}}"
      puts CLI::UI.fmt "{{green: + #{d['value']} }}"
    when 'remove'
      puts CLI::UI.fmt "{{red: - #{d['was']} }}"
    else
      puts CLI::UI.fmt "{{x}} unknown operation: #{d['op']}"
    end
  end
end