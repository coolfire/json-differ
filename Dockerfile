FROM ruby:3-alpine

COPY . /app

WORKDIR /app

RUN bundle config set --local without 'development'
RUN bundle install

CMD ["bundle", "exec", "ruby", "differ.rb"]
